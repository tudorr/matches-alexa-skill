# MatchesFashion Alexa Skill

## Purpose

This is the API for the MatchesFashion Alexa Skill. It loads the MatchesFashion website in an iFrame and it updates the
URL based on a term passed through API calls.

## Running locally

Node version: 10

Install the dependencies
```
npm i
```

Running the app
```
npm run dev
```