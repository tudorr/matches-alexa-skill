const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const socketIo = require('socket.io');

const port = 3000;

const app = express();
app.use(bodyParser.json());

const server = http.createServer(app);
const io = socketIo(server);

io.on('connection', (socket) => {
  socket.join('listeners');
});

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/public/index.html');
});

app.post('/api/changeUrl', (req, res) => {
  const urlsMap = {
    'women jeans': 'https://shop.mango.com/gb/women/jeans_c99220156',
    'women bags': 'https://shop.mango.com/gb/women/bags_c18162733',
    'women shoes': 'https://shop.mango.com/gb/women/shoes_c10336952',
    'men jeans': 'https://shop.mango.com/gb/men/jeans_c23998484',
    'men bags': 'https://shop.mango.com/gb/men/bags_c24594145',
    'men shoes': 'https://shop.mango.com/gb/men/shoes_c26231156',
  };


  if (urlsMap[req.body.term]) {
    io.to('listeners').emit('changeUrl', { url: urlsMap[req.body.term] || null });
  }

  res.json({
    ok: true,
  });
});

server.listen(port, () => {
  console.log(`listening on port ${port}`);
});
