FROM node:10-alpine

# Set base dir
WORKDIR /app

# Copy app files
COPY . /app

# NPM set repo and install
RUN npm install

# Expose the App's PORT
EXPOSE 3000

# Start the app
CMD ["npm", "start"]